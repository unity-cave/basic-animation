﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Animation
{
    IDLE = 0,
    MOVE = 1,
    ATTACK = 2,
    DIE = 3,
}
