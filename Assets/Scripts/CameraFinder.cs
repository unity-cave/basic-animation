﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cave;

public class CameraFinder : MonoBehaviour {
    private Camera caveCamera;
    private Canvas thisCanvas;

	// Use this for initialization
	void Start () {
        GameObject origin = InstantiateNode.FindOrigin();
        if(origin == null){
            Debug.LogError("Couldn't find origin");
        }
        else{
            caveCamera = origin.GetComponentInChildren<Camera>();
			thisCanvas = this.GetComponent<Canvas>();
            if(caveCamera == null){
                Debug.LogError("Couldn't find CAVE camera");
            }
            else if(thisCanvas != null){
                thisCanvas.worldCamera = caveCamera;
			}
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
