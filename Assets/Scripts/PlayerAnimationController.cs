﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour
{
    #region Attributes
    private Animator animator;
    public Animation currentAnimation = Animation.IDLE;
    private Animation lastAnimation;
    #endregion

    void Start(){
        animator = GetComponent<Animator>();
        lastAnimation = currentAnimation;
    }

    public void AnimateIdle(){
        currentAnimation = Animation.IDLE;
    }
    public void AnimateMove()
    {
        currentAnimation = Animation.MOVE;
    }
    public void AnimateAttack()
    {
        currentAnimation = Animation.ATTACK;
    }
    public void AnimateDeath()
    {
        currentAnimation = Animation.DIE;
    }

    void Update(){
        if(currentAnimation != lastAnimation){
            Debug.LogWarning("Current animation: " + currentAnimation.ToString() + " last animation: " + lastAnimation.ToString());
            Animate();
            lastAnimation = currentAnimation;
        }
    }

    private void Animate(){
        animator.SetInteger("Animation", (int)currentAnimation);
    }
}
